"""
Contains a bunch of data models to use in this practical work
"""

# Data set of temperature according to the altitude during summer
summer_temperature = {
    "title": "Summer temperature",
    "altitude": [3500, 2800, 1300, 750, 300, 900, 1800, 3100],
    "temperature": [10, 13, 20, 25, 30, 22, 18, 11]
}

# Data set of temperature according to the altitude during winter
winter_temperature = {
    "title": "Winter temperature",
    "altitude": [3500, 2800, 1300, 750, 300, 900, 1800, 3100],
    "temperature": [-15, -11, 0, 3, 10, 2, -2, -13]
}

# 300m <=> 15°C
mod_summer_temperature = {
    "title": "Summer temperature",
    "altitude": [3500, 2800, 1300, 750, 300, 900, 1800, 3100],
    "temperature": [10, 13, 20, 25, 15, 22, 18, 11]
}

# 300m <=> 15°C
mod_winter_temperature = {
    "title": "Winter temperature",
    "altitude": [3500, 2800, 1300, 750, 300, 900, 1800, 3100],
    "temperature": [-15, -11, 0, 3, 15, 2, -2, -13]
}

# Drivers reacting time's random variable
drivers_reacting_time = [.98, 1.4, .84, .88, .54, .68, 1.35, .76, .72, .99, .88, .75,
                         .49, 1.09, .68, .60, 1.13, 1.35, 1.13, .91]

# The students proportion who studied Algorithmic's random variable
students_proportion = [1 if i < 637 else 0 for i in range(1_000)]
