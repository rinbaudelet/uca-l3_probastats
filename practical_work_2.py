import math

import numpy.random

from models import students_proportion, drivers_reacting_time
from probastats import poisson, binomial, normal_law, mean, confidence_interval, \
    empirical_variance


def build_simulation(simulation, size, min_value=None, max_value=None) -> []:
    if min_value is None:
        min_value = min(simulation)
    if max_value is None:
        max_value = max(simulation) + 1

    occurencies_sim = {val: 0 for val in range(min_value, max_value)}
    for val in simulation:
        occurencies_sim[val] += 1
    occurencies_list = []
    for index in sorted(occurencies_sim):
        occurencies_list.append(occurencies_sim[index] / size)
    return occurencies_list


def simulate_poisson(lmbda: int, n_sim: int = 10_000):
    """

    :param lmbda:
    :param n_sim:
    """
    import matplotlib.pyplot as plt

    simulate = numpy.random.poisson(lmbda, n_sim)
    min_value, max_value = min(simulate), max(simulate) + 1
    poisson_vec = [poisson(lmbda, i) for i in range(min_value, max_value)]
    occurencies_list = build_simulation(simulate, n_sim, min_value, max_value)

    plt.bar([index for index in range(min_value, max_value)], occurencies_list,
            label='Simulate values')
    plt.plot([index for index in range(min_value, max_value)], poisson_vec,
             'ro', label='Theorical values')
    plt.legend()
    plt.title(f'Lois discrètes de poisson λ={lmbda} with n={n_sim} simulations')
    plt.xlabel(f'k')
    plt.ylabel(f'P[X = k]')
    plt.savefig(f'resources/pw2/poisson_lmbda={lmbda}_n={n_sim}.png')
    plt.show()


def simulate_binomial(n: int, p: float, n_sim=10_000):
    import matplotlib.pyplot as plt

    simulate = numpy.random.binomial(n, p, n_sim)
    binomial_vec = [binomial(n, p, k) for k in range(n)]
    occurencies_list = build_simulation(simulate, n_sim, 0, n)

    plt.bar([index for index in range(n)], occurencies_list,
            label='Simulate values')
    plt.plot([index for index in range(n)], binomial_vec,
             'ro', label='Theorical values')
    plt.legend()
    plt.title(f'Loi binomiale (n={n}, p={p})')
    plt.xlabel(f'k')
    plt.ylabel(f'P[X = k]')
    plt.savefig(f'resources/pw2/binomial_n={n}_p={p}.png')
    plt.show()


def simulate_normal_law(_mean, _std_deviation, size=1_000):
    pass


def reacting_time():
    random_var = drivers_reacting_time

    print('Reacting time:')
    print(f'\t- with variance = 0.2:')
    print(f'\t\t- empirical mean: {mean(random_var)}')
    print(f'\t\t- confidence interval (95%): ['
          f'{confidence_interval(random_var, .2, 1.7247)}]')
    print(f'\t\t- confidence interval (99%): ['
          f'{confidence_interval(random_var, .2, 2.528)}]')
    print(f'\t- using empirical variance:')
    emp_var = empirical_variance(random_var)
    print(f'\t\t- confidence interval (95%): ['
          f'{confidence_interval(random_var, emp_var, 1.7247)}]')
    print(f'\t\t- confidence interval (99%): ['
          f'{confidence_interval(random_var, emp_var, 2.528)}]')


def proportion_est():
    random_var = students_proportion
    emp_var = empirical_variance(random_var)
    print("Estimation of a proportion:")
    print(f"\t- interval (95%): [{confidence_interval(random_var, emp_var, 1.6449)}]")


def main():
    print('='*10, 'PRACTICAL WORK #2', '='*10)
    for lam in [1, 10, 30]:
        simulate_poisson(lam)
    for n, p in [(50, .5), (50, .7), (50, .2)]:
        simulate_binomial(n, p)
    simulate_normal_law(25, math.sqrt(50*0.25))
    reacting_time()
    proportion_est()
    print('='*39)


if __name__ == '__main__':
    main()
