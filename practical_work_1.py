from models import winter_temperature, summer_temperature, mod_summer_temperature, \
    mod_winter_temperature
from probastats import *


def do_approximation(data: dict, method) -> dict:
    import matplotlib.pyplot as plt

    data_x, data_y = data["altitude"], data["temperature"]
    a, b = method(data_x, data_y)
    plt_x = np.linspace(min(data_x), max(data_x), 200)
    plt_y = a * plt_x + b
    est_y = [a * x + b for x in data_x]

    # Compute the maximum likehood estimation to modelize the error margin
    sigma = math.sqrt(max_likehood_est(data_x, data_y, a, b))
    deviation = rmse(data_y, est_y)
    deter_coeff = determination_coeff(data_y, est_y, mean(data_y))

    # setup plt
    plt.title(f"Linear Regression of {data['title']} using {method.__name__}")
    plt.ylabel("Temperature (°C)")
    plt.xlabel("Altitude")
    plt.plot(plt_x, plt_y, label=data['title'])
    plt.plot(plt_x, plt_y-sigma, label='Low estimation')
    plt.plot(plt_x, plt_y+sigma, label='High estimation')
    plt.legend()
    plt.savefig(f'resources/pw1/reg-{data["title"]}-{method.__name__}.png')
    plt.show()

    return {
        "factor": {
            "a": a,
            "b": b
        },
        "data_x": data_x,
        "data_y": data_y,
        "data_title": data['title'],
        "RMSE": deviation,
        "sigma": sigma,
        "R²": deter_coeff,
        "method": method.__name__
    }


def get_temperature(data: dict, altitude):
    x, y = data["altitude"], data['temperature']
    a, b = least_squares(x, y)
    return a * altitude + b


def main():
    print('='*10, 'PRACTICAL WORK #1', '='*10)
    for data in [winter_temperature, summer_temperature]:
        results = []
        for method in [least_squares, linear_regression]:
            results.append(do_approximation(data, method))
            print(f'Result for {data["title"]} using {method.__name__}:')
            print(results[len(results)-1])

    print('Temperature at 1000m:')
    print(f'\tDuring summer: {get_temperature(summer_temperature, 1000)}°C')
    print(f'\t During winter: {get_temperature(winter_temperature, 1000)}°C')
    print('Temperature at 1000m w/ 15°C at 300m:')
    print(f'\tDuring summer: {get_temperature(mod_summer_temperature, 1000)}°C')
    print(f'\tDuring winter: {get_temperature(mod_winter_temperature, 1000)}°C')
    print('='*39)


if __name__ == '__main__':
    main()
