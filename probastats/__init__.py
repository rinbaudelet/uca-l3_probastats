"""
Package containing useful functions for this practical work
"""
import math
import numpy as np


def least_squares(x: list, y: list, ret_value: bool = True):
    """
    Execute the method of the least squares according to a list of values to compute
    the two factors (a, b).

    :param x: List of values on the x-axis
    :param y: List of values on the y-axis
    :param ret_value: Specify if the function does return a tuple (a, b) or the lambda
    function f(x) = ax+b
    :return: The compute values (a, b) or the function f(x) = ax+b
    """
    n = len(x)
    if n != len(y):
        raise Exception("Invalid argument (incompatible size)")

    def sum_of_mul(data_list: [[]]):
        add = 0
        for i in range(len(data_list[0])):
            mul = 1
            for lst in data_list:
                mul *= lst[i]
            add += mul
        return add

    a = ((n * sum_of_mul([x, y])) - (sum(x) * sum(y))) / (
            n * sum_of_mul([x, x]) - sum(x) ** 2)
    b = sum(y) / n - a * (sum(x) / n)
    return (a, b) if ret_value else lambda var: a * var + b


def max_likehood_est(x: list, y: list, a: float, b: float) -> float:
    """
    Compute the maximum likehood estimation from the function y = f(x) from
    a list of given values

    :param x: a list of x-label
    :param y: a list of values like y = f(x)
    :param a: the a-factor of the least_squares of y = f(x)
    :param b: the b-factor of the least_squares of y = f(x)
    :return: the sigma² of the max likehood estimation of y = f(x)
    """
    return sum([(y[i] - a * x[i] - b) ** 2 for i in range(len(x))]) / len(x)


def optimization_method(x: [int], y: [int]):
    """
    Execute the optimization method to compute the a, b factors to obtain
    an y = f(x) = a * x + b from a list of values.

    :param x: a list of values on the x-axis
    :param y: a list of values on the y-axis
    :return: The compute values (a, b)
    """
    n, eps, gamma = len(x), np.exp(-3, dtype=np.float128), np.float128(.5)
    x = np.array(x, dtype=np.float128)
    y = np.array(y, dtype=np.float128)

    def min_j(a, b) -> np.float128:
        return np.sum([(a * x[i] + b - y[i]) for i in range(n)],
                      dtype=np.float128)

    def grad_a(a, b) -> np.float128:
        return np.sum([x[i] * (- y[i] + a * x[i] + b) for i in range(n)],
                      dtype=np.float128) / np.float128(n)

    def grad_b(a, b) -> np.float128:
        return np.sum([a * x[i] + b - y[i] for i in range(n)],
                      dtype=np.float128) / np.float128(n)

    val_a = -gamma * grad_a(0, 0)
    val_b = -gamma * grad_b(0, 0)
    gap = min_j(val_a, val_b) - min_j(0, 0)

    while abs(gap) >= eps:
        gap = -min_j(val_a, val_b)
        va, vb = val_a, val_b
        val_b -= gamma * grad_b(va, vb)
        val_a -= gamma * grad_a(va, vb)

    return val_a, val_b


def linear_regression(x: [int], y: [int]) -> (float, float):
    """
    Execute the sklearn.linear_model.LinearRegression tool
    :param x: a list of values on the x-axis
    :param y: a list of values on the y-axis
    :return: The compute values (a, b)
    """
    from sklearn.linear_model import LinearRegression
    regression = LinearRegression()
    x = np.array(x).reshape(-1, 1)
    y = np.array(y).reshape(-1, 1)

    regression.fit(x, y)
    return regression.coef_[0], regression.intercept_[0]


def rmse(observe_y: list, estimate_y: list) -> float:
    """
    Compute the root mean-squre deviation
    :param observe_y: the list of observe values
    :param estimate_y: the list of estimate values
    :return: the RMSE
    """
    return math.sqrt(
        sum([(estimate_y[i] - observe_y[i]) ** 2 for i in
             range(len(observe_y))]) / len(observe_y)
    )


def determination_coeff(observe_y: list, estimate_y: list, mean_y: float) -> float:
    """
    Compute the determination's coeff R²

    :param observe_y: the list of observe values
    :param estimate_y: te list of estimate values
    :param mean_y: the mean
    :return: R²
    """
    return sum([(estimate_y[i] - mean_y) ** 2 for i in range(len(observe_y))]) \
        / sum([(observe_y[i] - mean_y) ** 2 for i in range(len(observe_y))])


def mean(values: []):
    """
    Get the mean of an array of values
    :param values: data to compute
    :return: the mean of the values
    """
    return sum(values) / len(values)


def poisson(lmbda: float, i: int):
    """
    Get the poisson law P[X = i] with set λ
    :param lmbda: the value of λ
    :param i: X = i
    :return: the value of the poisson law P[X = i] with λ = lambda
    """
    return np.exp(-lmbda) * ((lmbda ** i) / math.factorial(i))


def binomial(n: int, p: float, k: int):
    return math.comb(n, k) * (p ** k) * ((1 - p) ** (n - k))


def normal_law(x, _mean, std_deviation):
    """
    Get the normal law P[X = x] with set µ, σ²
    :param x:
    :param _mean:
    :param std_deviation:
    :return:
    """
    return (1 / (std_deviation * math.sqrt(2 * math.pi))) * math.exp(
        -.5 * ((x - _mean) / std_deviation) ** 2)


def confidence_interval(random_var, variance, st) -> (float, float):
    """
    Compute the confidence interval of a random variable following a normal law
    :param random_var: the random variable
    :param variance: the variance
    :param st: the student law parameter according to the confidence percentage
    :return: a tuple containing the interval (low, high)
    """
    mean_random_var = mean(random_var)
    return mean_random_var - st * (variance / math.sqrt(len(random_var))), \
           mean_random_var + st * (variance / math.sqrt(len(random_var)))


def empirical_variance(random_var: []):
    """
    Compute the empirical variance of the passing random variable
    :param random_var: the random variable to use to compute the empirical variance
    :return: the empirical variance of the random variable
    """
    n = len(random_var)
    return sum([(random_var[i] - mean(random_var)) ** 2 for i in range(n)]) / (n - 1)
